module gitlab.com/gitlab-org/security-products/analyzers/sobelow/v2

go 1.12

require (
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.6.1
)

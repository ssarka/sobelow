package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v2/plugin"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "sobelow analyzer for GitLab SAST"
	app.Author = "GitLab"

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert.Convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

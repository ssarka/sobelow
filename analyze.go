package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const (
	ignoredModules = "Config,Vuln" // disable config and dependency_scanning modules
	pathOutput     = "sobelow.json"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	args := []string{
		"sobelow",
		"--private", // disable outbound version check
		"--format", "json",
		"--ignore", ignoredModules,
		"--out", pathOutput, // set output file
	}

	err := setupCmd(exec.Command("mix", args...)).Run()
	if err != nil {
		return nil, err
	}

	return os.Open(filepath.Join(path, pathOutput))
}

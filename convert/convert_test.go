package convert

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestConvert(t *testing.T) {
	in := `{
		"findings": {
			 "high_confidence": [
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 10,
						"type": "XSS.ContentType: XSS in ` + "`put_resp_content_type`" + `",
						"variable": "unsafe_param"
					},
					{
						"file": "config/prod.exs",
						"line": 0,
						"type": "Config.HTTPS: HTTPS Not Enabled"
					},
					{
						"file": "config/prod.secret.exs",
						"key": "secret_key_base",
						"line": 12,
						"type": "Config.Secrets: Hardcoded Secret"
					},
					{
						"file": "lib/sample_app_web/router.ex",
						"line": 4,
						"pipeline": "browser",
						"type": "Config.Headers: Missing Secure Browser Headers"
					},
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 16,
						"type": "SQL.Query: SQL injection",
						"variable": "sql"
					},
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 24,
						"type": "RCE.EEx: Code Execution in ` + "`EEx.eval_string`" + `",
						"variable": "unsafe_param"
					},
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 32,
						"type": "DOS.StringToAtom: Unsafe ` + "`String.to_atom`" + `",
						"variable": "unsafe_template"
					},
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 37,
						"type": "Command Injection in ` + "`:os.cmd`" + `",
						"variable": "unsafe_cmd"
					},
					{
						"file": "lib/sample_app_web/router.ex",
						"line": 4,
						"pipeline": "browser",
						"type": "Config.CSRF: Missing CSRF Protections"
					}
				],
				"low_confidence": [
				],
				"medium_confidence": [
				]
		},
		"sobelow_version": "0.8.0",
		"total_findings": 9
	}`

	scanner := issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	r := strings.NewReader(in)
	want := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "XSS in `put_resp_content_type`",
				Message:     "XSS in `put_resp_content_type`",
				Description: "Cross-Site Scripting (XSS) vulnerabilities are a result of rendering untrusted input on a page without proper encoding. XSS may allow an attacker to perform actions on behalf of other users, steal session tokens, or access private data. Read more about XSS here: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:10:xss_in_put_resp_content_type",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelHigh,
				Location: issue.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 10,
				},
				Identifiers: []issue.Identifier{
					issue.Identifier{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID xss_in_put_resp_content_type",
						Value: "xss_in_put_resp_content_type",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "SQL injection",
				Message:     "SQL injection",
				Description: "SQL injection occurs when untrusted input is interpolated directly into a SQL query. In a typical Phoenix application, this would mean using the `Ecto.Adapters.SQL.query` method and not using the parameterization feature. Read more about SQL injection here: https://www.owasp.org/index.php/SQL_Injection",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:16:sql_injection",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelHigh,
				Scanner:     scanner,
				Location: issue.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 16,
				},
				Identifiers: []issue.Identifier{
					issue.Identifier{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID sql_injection",
						Value: "sql_injection",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "Code Execution in `EEx.eval_string`",
				Message:     "Code Execution in `EEx.eval_string`",
				Description: "Insecure EEx evaluation. If user input is passed to EEx eval functions, it may result in arbitrary code execution. The root cause of these issues is often directory traversal.",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:24:rce_in_eex_eval_string",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelHigh,
				Scanner:     scanner,
				Location: issue.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 24,
				},
				Identifiers: []issue.Identifier{
					issue.Identifier{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID rce_in_eex_eval_string",
						Value: "rce_in_eex_eval_string",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "Unsafe `String.to_atom`",
				Message:     "Unsafe `String.to_atom`",
				Description: "Denial of Service via `String.to_atom` In Elixir, atoms are not garbage collected. As such, if user input is passed to the `String.to_atom` function, it may result in memory exhaustion. Prefer the `String.to_existing_atom` function for untrusted user input.",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:32:dos_unsafe_string_to_atom",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelHigh,
				Scanner:     scanner,
				Location: issue.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 32,
				},
				Identifiers: []issue.Identifier{
					issue.Identifier{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID dos_unsafe_string_to_atom",
						Value: "dos_unsafe_string_to_atom",
					},
				},
			},
			issue.Issue{
				Category:    "sast",
				Name:        "Command Injection in `:os.cmd`",
				Message:     "Command Injection in `:os.cmd`",
				Description: "Command Injection vulnerabilities are a result of passing untrusted input to an operating system shell, and may result in complete system compromise. Read more about Command Injection here: https://www.owasp.org/index.php/Command_Injection",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:37:ci_in_os_cmd",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelHigh,
				Scanner:     scanner,
				Location: issue.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 37,
				},
				Identifiers: []issue.Identifier{
					issue.Identifier{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID ci_in_os_cmd",
						Value: "ci_in_os_cmd",
					},
				},
			},
		},
		DependencyFiles: []issue.DependencyFile{},
		Remediations:    []issue.Remediation{},
	}

	got, err := Convert(r, "")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

package convert

import (
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

type SoBelowReport struct {
	Findings       SoBelowConfidenceGroups `json:"findings"`
	SoBelowVersion string                  `json:"sobelow_version"`
	TotalFindings  int                     `json:"total_findings"`
}

type SoBelowConfidenceGroups struct {
	HighConfidence   []SoBelowVulnerability `json:"high_confidence"`
	MediumConfidence []SoBelowVulnerability `json:"medium_confidence"`
	LowConfidence    []SoBelowVulnerability `json:"low_confidence"`
}

type SoBelowVulnerability struct {
	Type       string
	File       string
	Line       int
	Confidence issue.ConfidenceLevel
}

func (r *SoBelowReport) vulnerabilities() []SoBelowVulnerability {
	vulnerabilityCount := len(r.Findings.HighConfidence) + len(r.Findings.MediumConfidence) + len(r.Findings.LowConfidence)
	vulnerabilities := make([]SoBelowVulnerability, vulnerabilityCount)

	for _, vulnerability := range r.Findings.HighConfidence {
		vulnerability.Confidence = issue.ConfidenceLevelHigh
		vulnerabilities = append(vulnerabilities, vulnerability)
	}
	for _, vulnerability := range r.Findings.MediumConfidence {
		vulnerability.Confidence = issue.ConfidenceLevelMedium
		vulnerabilities = append(vulnerabilities, vulnerability)
	}
	for _, vulnerability := range r.Findings.LowConfidence {
		vulnerability.Confidence = issue.ConfidenceLevelLow
		vulnerabilities = append(vulnerabilities, vulnerability)
	}

	return vulnerabilities
}

// compareKey returns a string used to establish whether two issues are the same.
func (v *SoBelowVulnerability) compareKey() string {
	attrs := []string{v.File, strconv.Itoa(v.Line), v.typeSlug()}
	return strings.Join(attrs, ":")
}

func (v *SoBelowVulnerability) typeSlug() string {
	return typeSlugs[v.Type]
}

func (v *SoBelowVulnerability) message() string {
	if v.Type == "Command Injection in `:os.cmd`" ||
		v.Type == "Command Injection in `System.cmd`" ||
		v.Type == "Insecure use of `File` and `Path`" {
		return v.Type
	}

	withoutType := strings.Split(v.Type, ": ")[1:]
	return strings.Join(withoutType, "")
}

// description returns a description if type is supported
func (v *SoBelowVulnerability) description() (string, bool) {
	desc, ok := descriptions[v.typeSlug()]
	return desc, ok
}

var typeSlugs = map[string]string{
	// Command Injection
	"Command Injection in `:os.cmd`":    "ci_in_os_cmd",
	"Command Injection in `System.cmd`": "ci_in_system_cmd",

	// // Config rules are not currently supported
	// "Hardcoded Secret":                "config_hardcoded_secret",
	// "HTTPS Not Enabled":               "config_https_not_enabled",
	// "Missing Content-Security-Policy": "config_missing_content_security_policy",
	// "Missing CSRF Protections":        "config_missing_csrf_protections",
	// "Missing Secure Browser Headers":  "config_missing_secure_browser_headers",

	// Denial of Service
	"DOS.ListToAtom: Unsafe `List.to_atom`":     "dos_unsafe_list_to_atom",
	"DOS.StringToAtom: Unsafe `String.to_atom`": "dos_unsafe_string_to_atom",
	"DOS.BinToAtom: Unsafe atom interpolation":  "dos_unsafe_atom_interpolation",

	// Misc
	"Misc.BinToTerm: Unsafe `binary_to_term`": "misc_unsafe_binary_to_term",
	"Insecure use of `File` and `Path`":       "misc_insecure_use_of_file_and_path",

	// RCE
	"RCE.EEx: Code Execution in `EEx.eval_file`":           "rce_in_eex_eval_file",
	"RCE.EEx: Code Execution in `EEx.eval_string`":         "rce_in_eex_eval_string",
	"RCE.CodeModule: Code Execution in `Code.eval_file`":   "rce_in_code_eval_file",
	"RCE.CodeModule: Code Execution in `Code.eval_quoted`": "rce_in_code_eval_quoted",
	"RCE.CodeModule: Code Execution in `Code.eval_string`": "rce_in_code_eval_string",

	// SQL
	"SQL.Query: SQL injection":  "sql_injection",
	"SQL.Stream: SQL injection": "sql_injection",

	// Traversal
	"Traversal.SendDownload: Directory Traversal in `send_download`": "traversal_in_send_download",
	"Traversal.SendFile: Directory Traversal in `send_file`":         "traversal_in_send_file",

	// XSS
	"XSS.Raw: XSS":            "xss",
	"XSS.HTML: XSS in `html`": "xss_in_html",
	"XSS.ContentType: XSS in `put_resp_content_type`": "xss_in_put_resp_content_type",
}

// These descriptions are taken from Sobelow's moduledocs; i.e.
// https://github.com/nccgroup/sobelow/blob/v0.7.3/lib/sobelow/rce.ex#L2
var descriptions = map[string]string{
	"ci_in_os_cmd":                       "Command Injection vulnerabilities are a result of passing untrusted input to an operating system shell, and may result in complete system compromise. Read more about Command Injection here: https://www.owasp.org/index.php/Command_Injection",
	"ci_in_system_cmd":                   "Command Injection vulnerabilities are a result of passing untrusted input to an operating system shell, and may result in complete system compromise. Read more about Command Injection here: https://www.owasp.org/index.php/Command_Injection",
	"dos_unsafe_atom_interpolation":      "Denial of Service via Unsafe Atom Interpolation In Elixir, atoms are not garbage collected. As such, if user input is used to create atoms (as in " + `:"foo\#{bar}"` + ", or in `:erlang.binary_to_atom`), it may result in memory exhaustion. Prefer the `String.to_existing_atom` function for untrusted user input.",
	"dos_unsafe_list_to_atom":            "Denial of Service via `List.to_atom` In Elixir, atoms are not garbage collected. As such, if user input is passed to the `List.to_atom` function, it may result in memory exhaustion. Prefer the `List.to_existing_atom` function for untrusted user input.",
	"dos_unsafe_string_to_atom":          "Denial of Service via `String.to_atom` In Elixir, atoms are not garbage collected. As such, if user input is passed to the `String.to_atom` function, it may result in memory exhaustion. Prefer the `String.to_existing_atom` function for untrusted user input.",
	"misc_insecure_use_of_file_and_path": "Insecure use of `File` and `Path` In Elixir, `File` methods are null-terminated, while `Path` functions are not. This may cause security issues in certain situations.",
	"misc_unsafe_binary_to_term":         "Insecure use of `binary_to_term` If user input is passed to Erlang's `binary_to_term` function it may result in memory exhaustion or code execution. Even with the `:safe` option, `binary_to_term` will deserialize functions, and shouldn't be considered safe to use with untrusted input.",
	"rce_in_eex_eval_file":               "Insecure EEx evaluation. If user input is passed to EEx eval functions, it may result in arbitrary code execution. The root cause of these issues is often directory traversal.",
	"rce_in_eex_eval_string":             "Insecure EEx evaluation. If user input is passed to EEx eval functions, it may result in arbitrary code execution. The root cause of these issues is often directory traversal.",
	"rce_in_code_eval_file":              "Remote Code Execution vulnerabilities are a result of untrusted user input being executed or interpreted by the system and may result in complete system compromise.",
	"rce_in_code_eval_quoted":            "Remote Code Execution vulnerabilities are a result of untrusted user input being executed or interpreted by the system and may result in complete system compromise.",
	"rce_in_code_eval_string":            "Remote Code Execution vulnerabilities are a result of untrusted user input being executed or interpreted by the system and may result in complete system compromise.",
	"sql_injection":                      "SQL injection occurs when untrusted input is interpolated directly into a SQL query. In a typical Phoenix application, this would mean using the `Ecto.Adapters.SQL.query` method and not using the parameterization feature. Read more about SQL injection here: https://www.owasp.org/index.php/SQL_Injection",
	"traversal_in_send_download":         "Path traversal vulnerabilities are a result of interacting with the filesystem using untrusted input. This class of vulnerability may result in file disclosure, code execution, denial of service, and other issues. Read more about Path Traversal here: https://www.owasp.org/index.php/Path_Traversal",
	"traversal_in_send_file":             "Path traversal vulnerabilities are a result of interacting with the filesystem using untrusted input. This class of vulnerability may result in file disclosure, code execution, denial of service, and other issues. Read more about Path Traversal here: https://www.owasp.org/index.php/Path_Traversal",
	"xss":                                "Cross-Site Scripting (XSS) vulnerabilities are a result of rendering untrusted input on a page without proper encoding. XSS may allow an attacker to perform actions on behalf of other users, steal session tokens, or access private data. Read more about XSS here: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)",
	"xss_in_html":                        "Cross-Site Scripting (XSS) vulnerabilities are a result of rendering untrusted input on a page without proper encoding. XSS may allow an attacker to perform actions on behalf of other users, steal session tokens, or access private data. Read more about XSS here: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)",
	"xss_in_put_resp_content_type":       "Cross-Site Scripting (XSS) vulnerabilities are a result of rendering untrusted input on a page without proper encoding. XSS may allow an attacker to perform actions on behalf of other users, steal session tokens, or access private data. Read more about XSS here: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)",
}

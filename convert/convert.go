package convert

import (
	"encoding/json"
	"io"
	"fmt"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	scannerID   = "sobelow"
	scannerName = "Sobelow"
)

// Convert reads a native report from the tool (sobelow) and transforms it
// into issues (vulnerabilities), as defined in the common module
func Convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	var fileReport SoBelowReport

	err := json.NewDecoder(reader).Decode(&fileReport)
	if err != nil {
		return nil, err
	}

	scanner := issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	// Create issues from sobelow report
	issues := make([]issue.Issue, 0)

	for _, vulnerability := range fileReport.vulnerabilities() {
		description, ok := vulnerability.description()
		if !ok {
			// Ignore unknown rule types
			continue
		}

		newIssue := issue.Issue{
			Category:    issue.CategorySast,
			Scanner:     scanner,
			Name:        vulnerability.message(),
			Message:     vulnerability.message(),
			Description: description,
			CompareKey:  vulnerability.compareKey(),
			Severity:    issue.SeverityLevelUnknown,
			Confidence:  vulnerability.Confidence,
			Location: issue.Location{
				File:      vulnerability.File,
				LineStart: vulnerability.Line,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "sobelow_rule_id",
					Name:  fmt.Sprintf("Sobelow Rule ID %s", vulnerability.typeSlug()),
					Value: vulnerability.typeSlug(),
				},
			},
		}

		issues = append(issues, newIssue)
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}

# Sobelow SAST analyzer changelog

## v2.2.0
- Add support for custom CA certs (!14)

## v2.1.1
- Upgrade elixir base image to 1.9.4

## v2.1.0
- Upgrade to [Sobelow 0.8.0](https://github.com/nccgroup/sobelow/blob/master/CHANGELOG.md#v080)
- Fix link to affected line of the source code
- Change compare keys, use fixed line number and remove function name

## v2.0.1
- Define MIX_HOME in Dockerfile to prevent dynamic definition at runtime

## v2.0.0
- Initial release
